
vm = this;
const recipeId = localStorage.getItem('recipe_id');
getRecipe(recipeId);

// get details of recipe
function getRecipe(id) {
    fetch('http://localhost:3001/recipes/' + id)
        .then(res => res.json())
        .then(res => {
            getSpecials(res);
        });
}

// get list of specials
function getSpecials(recipe) {
    return fetch('http://localhost:3001/specials')
        .then(res => res.json())
        .then(res => {
            vm.listSpecials = res;
            bindSpecialsToRecipe(res, recipe);
        });
}

// bind specials if ingredientId match to ingredient uuid
function bindSpecialsToRecipe(res, recipe) {
    recipe.ingredients.map((el) => {
        el.special = findIfhasSpecial(el, res);
    });
    populateData(recipe); //bind recipe details to html
}

// find if ingredientId match to ingredient uuid
function findIfhasSpecial(ingredient, specials) {
    let special = null;
    specials.map((sp) => {
        if (ingredient.uuid === sp.ingredientId) {
            special = sp;
        }
    });
    return special;
}

// bind recipe details to html
function populateData(data) {
    vm.selectedRecipe = data;
    document.getElementById("title").value = data.title;
    document.getElementById("servings").value = data.servings;
    document.getElementById("prepTime").value = data.prepTime;
    document.getElementById("cookTime").value = data.cookTime;
    document.getElementById("description").value = data.description;

    vm.ingredients = data.ingredients;
    vm.directions = data.directions;

    document.getElementById("ingredientList").innerHTML = ingredientsList(vm.ingredients);
    document.getElementById("directionsList").innerHTML = directionsList(vm.directions);
}

// add special to selected ingredient
function addSpecialTag() {
    let special = {
        title: document.getElementById("titleSpecial").value,
        type: document.getElementById("typeSpecial").value,
        text: document.getElementById("descriptionSpecial").value,
        uuid: genUuid(),
        ingredientId: vm.selectedIngredient.uuid
    };

    vm.ingredients.map((ing) => {
        if (ing.uuid === vm.selectedIngredient.uuid) { //update special if ingredient have a special object
            ing.special = special;
        }
    });
    document.getElementById("ingredientList").innerHTML = ingredientsList(vm.ingredients);
    resetSpecialForm(); //reser form in html
}

function updateRecipe() {
    let specials = [];
    vm.ingredients.map((ing) => {
        delete ing.index;
        if (ing.special) { // if special exist in ingredient
            specials.push(ing.special);
            delete ing.special;
        }
    });

    vm.directions.map((el) => {
        delete el.index;
    });

    if (specials.length > 0) {
        editSpecials(specials); // check for modification of specials under the ingredient
    }

    const data = {
        uuid: vm.selectedRecipe.uuid,
        title: document.getElementById("title").value,
        servings: parseFloat(document.getElementById("servings").value),
        prepTime: parseFloat(document.getElementById("prepTime").value),
        cookTime: parseFloat(document.getElementById("cookTime").value),
        description: document.getElementById("description").value,
        images: vm.selectedRecipe.images,
        ingredients: vm.ingredients,
        directions: vm.directions
    };

    const options = requestOptions('PUT', data);
    fetch('http://localhost:3001/recipes/' + data.uuid, options) // request to update recipe 
        .then(res => res.json())
        .then(res => {
            console.log(res);
            window.location.href = "../../index.html";
        });
}

function editSpecials(specials) {
    let listOfSpecialModified = [];
    let listOfSpecialsNew = [];

    specials.map((sp) => {
        sp = validateSpecial(sp); //validate special if new or not
        if (sp.hasExist) {
            listOfSpecialModified.push(sp);
        }

        if (!sp.hasExist) {
            listOfSpecialsNew.push(sp);
        }
    });

    if (listOfSpecialModified.length > 0) {
        for (n in listOfSpecialModified) {
            delete listOfSpecialModified[n].hasExist;
            updateSpecial(listOfSpecialModified[n]);
        }
    }

    if (listOfSpecialsNew.length > 0) {
        for (a in listOfSpecialsNew) {
            delete listOfSpecialsNew[a].hasExist;
            postSpecial(listOfSpecialsNew[a]);
        }
    }
}

function validateSpecial(special) {
    vm.listSpecials.map((sp) => {
        special.hasExist = false;
        if (special.ingredientId === sp.ingredientId) {
            special.uuid = sp.uuid;
            special.hasExist = true;
        }
    });
    return special;
}

function updateSpecial(data) {
    const options = requestOptions('PUT', data);
    return fetch('http://localhost:3001/specials/' + data.uuid, options)
        .then(res => res.json())
        .then(res => {
            console.log(res);
        });
}