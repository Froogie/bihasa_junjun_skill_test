vm = this;
const recipeId = localStorage.getItem('recipe_id');

getRecipe(recipeId);

// get recipe details
function getRecipe(id) {
    return fetch('http://localhost:3001/recipes/' + id)
        .then(res => res.json())
        .then(res => {
            getSpecials(res);
        });
}

function getSpecials(recipe) {
    return fetch('http://localhost:3001/specials')
        .then(res => res.json())
        .then(res => {
            bindSpecialsToRecipe(res, recipe);
        });
}

function bindSpecialsToRecipe(res, recipe) {
    recipe.ingredients.map((el) => {
        el.special = findIfhasSpecial(el, res);
    });
    populateData(recipe);
}

// find ingredient if has special
function findIfhasSpecial(ingredient, specials) {
    let special = null;
    specials.map((sp) => {
        if (ingredient.uuid === sp.ingredientId) {
            special = sp;
        }
    });
    return special;
}

// populate recipe details data to html
function populateData(data) {
    vm.selectedRecipe = data;
    document.getElementById("title").value = data.title;
    document.getElementById("servings").value = data.servings;
    document.getElementById("prepTime").value = data.prepTime;
    document.getElementById("cookTime").value = data.cookTime;
    document.getElementById("description").value = data.description;
    document.getElementById("recipeImg").src = data.images.full;

    vm.ingredients = data.ingredients;
    vm.directions = data.directions;

    document.getElementById("ingredientList").innerHTML = listIngredients(vm.ingredients);
    document.getElementById("directionsList").innerHTML = directionsList(vm.directions);
}

// list of ingredients
function listIngredients(data) {
    var tableheader = `<tr>
                            <th>Ingredient Name</th>
                            <th>Measurement</th>
                            <th>Amount</th>
                         </tr>`;
    if (data.length > 0) {

        let table = "<table class='table table-sm table-striped'>";
        table += tableheader;

        data.map((obj) => {

            let special = '';

            if (obj.special) {
                special = "<br> <span class='span_text'>" + obj.special.title +
                    " (" + obj.special.type + ")" +
                    "<br>" + obj.special.text + "</span>";
            }

            table += "<tr>";
            table += "<td>" + obj.name + special + "</td>";
            table += "<td>" + obj.measurement + "</td>";
            table += "<td>" + obj.amount + "</td>";
            table += "</tr>";
        });

        table += "</table>";

        return table;
    } else {
        return '<p>No records!</p>';
    }
}