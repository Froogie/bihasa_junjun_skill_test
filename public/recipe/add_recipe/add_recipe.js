vm = this;

onLoadpage();

// onload of page populate blank arrays for display in ui
function onLoadpage() {
    vm.ingredients = [];
    vm.directions = [];
    vm.specials = [];

    document.getElementById("ingredientList").innerHTML = ingredientsList(vm.ingredients);
    document.getElementById("directionsList").innerHTML = directionsList(vm.directions);
}

// for reset of recipe form
function resetRecipeForm() {
    document.getElementById("title").value = null;
    document.getElementById("servings").value = 0;
    document.getElementById("prepTime").value = 0;
    document.getElementById("cookTime").value = 0;
    document.getElementById("description").value = null;
}

// for reset of ingredient form
function resetIngredientForm() {
    document.getElementById("ingredientName").value = null;
    document.getElementById("measurement").value = null;
    document.getElementById("amount").value = 0;
}

// for reset of directions/instructions form
function resetDirectionForm() {
    document.getElementById("instructions").value = null;
}

// for generation of unique uuid
function genUuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

function addIngredient() {
    const data = {
        name: document.getElementById("ingredientName").value,
        measurement: document.getElementById("measurement").value,
        amount: parseFloat(document.getElementById("amount").value),
        uuid: genUuid()
    };

    vm.ingredients.push(data);
    document.getElementById("ingredientList").innerHTML = ingredientsList(vm.ingredients);
    resetIngredientForm();
}

function addDirection() {
    const data = {
        instructions: document.getElementById("instructions").value,
        optional: false
    };

    vm.directions.push(data);
    document.getElementById("directionsList").innerHTML = directionsList(vm.directions);
    resetDirectionForm();
}

// list of ingredient
function ingredientsList(data) {
    var tableheader = `<tr>
                            <th>Ingredient Name</th>
                            <th>Measurement</th>
                            <th>Amount</th>
                            <th class='text-center'>Action</th>
                         </tr>`;
    if (data.length > 0) {

        let table = "<table class='table table-sm table-striped'>";
        table += tableheader;

        data.map((obj, index) => {
            let special = '';
            obj.index = index;

            if (obj.special) {
                special = "<br> <span class='span_text'>" + obj.special.title +
                    " (" + obj.special.type + ")" +
                    "<br>" + obj.special.text + "</span>";
            }

            table += "<tr>";
            table += "<td>" + obj.name + special + "</td>";
            table += "<td>" + obj.measurement + "</td>";
            table += "<td>" + obj.amount + "</td>";
            table += `<td class='text-center'>` + `<a onClick='removeIngredient(` + JSON.stringify(obj) +
                `)'><i title='Remove' class='colorRed fa fa-trash'></i></a>` +
                `&nbsp;<a onClick='getIngredientDetail(` + JSON.stringify(obj) + `)'
            data-toggle='modal', data-target='#specialModal'>
            <i title='Tag as Special' class='colorBlue fa fa-tag'></i></a> </td>`;
            table += "</tr>";
        });

        table += "</table>";

        return table;

    } else {
        return '<p>No records!</p>';
    }
}

// view ingredient detail
function getIngredientDetail(ingredient) {
    vm.selectedIngredient = ingredient;
}

// add special to ingredient
function addSpecial() {
    let special = {
        title: document.getElementById("titleSpecial").value,
        type: document.getElementById("typeSpecial").value,
        text: document.getElementById("descriptionSpecial").value,
        uuid: genUuid(),
        ingredientId: vm.selectedIngredient.uuid
    };

    vm.ingredients.map((ing) => {
        if (ing.uuid === vm.selectedIngredient.uuid) {
            ing.special = special;
        }
    });
    document.getElementById("ingredientList").innerHTML = ingredientsList(vm.ingredients);
    resetSpecialForm();
}

// reset special form
function resetSpecialForm() {
    document.getElementById("titleSpecial").value = null;
    document.getElementById("typeSpecial").value = null;
    document.getElementById("descriptionSpecial").value = null;
}

// remove ingredient in list of ingredients
function removeIngredient(obj) {
    vm.ingredients.map((ing, index) => {
        if (ing.uuid === obj.uuid) {
            vm.ingredients.splice(index, 1);
        }
    });
    document.getElementById("ingredientList").innerHTML = ingredientsList(vm.ingredients);
}

// list of instructions
function directionsList(data) {
    var tableheader = `<tr>
                            <th>Instructions</th>
                         </tr>`;
    if (data.length > 0) {

        let table = "<table class='table table-sm table-striped'>";
        table += tableheader;

        data.map((obj, index) => {
            obj.index = index;
            table += "<tr>";
            table += "<td>" + obj.instructions + "</td>";
            table += "</tr>";
        });

        table += "</table>";

        return table;
    } else {
        return '<p>No records!</p>';
    }
}

function insertRecipe() {
    let specials = [];
    var images = {
        full: '/img/default.png',
        medium: '/img/default.png',
        small: '/img/default.png',
    };

    vm.ingredients.map((ing) => {
        delete ing.index;
        if (ing.special) {
            specials.push(ing.special);
            delete ing.special;
        }
    });

    vm.directions.map((el) => {
        delete el.index;
    });

    const data = {
        uuid: genUuid(),
        title: document.getElementById("title").value,
        servings: parseFloat(document.getElementById("servings").value),
        prepTime: parseFloat(document.getElementById("prepTime").value),
        cookTime: parseFloat(document.getElementById("cookTime").value),
        description: document.getElementById("description").value,
        images: images,
        ingredients: vm.ingredients,
        directions: vm.directions
    };

    const options = requestOptions('POST', data);
    fetch('http://localhost:3001/recipes', options)
        .then(res => res.json())
        .then(res => {
            console.log(res);
            if (specials.length > 0) {
                insertSpecials(specials);
            }
            window.location.href = "../../index.html";
        });
}

function insertSpecials(specials) {
    for (n in specials) {
        postSpecial(specials[n]);
    }
}

function requestOptions(type, data) {
    return {
        method: type,
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    };
}

function postSpecial(data) {
    const options = requestOptions('POST', data);
    return fetch('http://localhost:3001/specials', options)
        .then(res => res.json())
        .then(res => console.log(res));
}

