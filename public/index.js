
(function getRecipeList() {
    fetch('http://localhost:3001/recipes')
        .then(res => res.json())
        .then(res => {
            console.log(res);
            document.getElementById("recipeList").innerHTML = recipeList(res);
        });
}());

function getSpecials() {
    fetch('http://localhost:3001/specials')
        .then(res => res.json())
        .then(res => {
            console.log(res);
        });
}

function recipeList(recipe_list) {
    var table = "<table class='table table-sm table-striped'>";

    table += `<tr>
                <th class='text-center'>Image</th>
                <th>Ingredient Name</th>
                <th>Description</th>
                <th class='text-center'>Serving/s</th>
                <th class='text-center'>Action</th>
              </tr>`;

    recipe_list.map((obj) => {
        table += "<tr>";
        table += "<td class='text-center'><img class='img_border_radius' width='50px' src='" + (obj.images.full || '/img/default.png') + "'></td>";
        table += "<td>" + obj.title + "</td>";
        table += "<td>" + obj.description + "</td>";
        table += "<td class='text-center'>" + obj.servings + "</td>";
        table += `<td class='text-center'>` + `<a onClick='getDetails(` + JSON.stringify(obj) +
            `)' href='./recipe/update_recipe/update_recipe.html'>Edit</a>` +
            ` <a onClick='getDetails(` + JSON.stringify(obj) +
            `)' href='./recipe/view_recipe/view_recipe.html'>View</a> </td>`;
        table += "</tr>";
    });

    table += "</table>";
    return table;
}

// get recipe details and save recipe uuid to local storage
function getDetails(data) {
    localStorage.setItem('recipe_id', data.uuid);
}
